# FbGAN-synthetic protein sequence generation.

*For optimal working of the FbGAN,one has to install all the python libraries that has been listed in requirements.txt file respectively. It's suggested to make a virtual environment for better implementation of the same.

*The code has been written in python 3x -(Python 3.6.3).

* Now onto the implementation, one has to run (wgan_gp_seq_generator.py) for synthetic sequence generation.
  
*all the dependencies for the above are being listed in folder "UTILS" respectively.

*all sort of moddifications for different metrics should be carried out on the main file itself.,i.e, (wgan_gp_seq_generator.py).

%% after complete execution of the code,following files would be generated.
* those are as follows:
                        1.)checkpoints.
                        2.)samples.

